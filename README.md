# Chat Application

## Table of Contents

- [Description](#description)
- [Commands](#commands)
    - [Docker containers management](#Docker containers management)
    - [Service level commands](#Service level commands)
- [Getting started](#getting-started)
    - [Environment configuration](#environment-configuration)
    - [Start application](#start-application)
- [FAQ](#faq)
    - [Entrypoint](#entrypoint)

## Description
Project is a micro-service dockerized stack developed on top of Vuejs framework.

Libraries are used:
- [TypeScript](https://www.typescriptlang.org/)
- [Vite](https://vitejs.dev/)
- [Pinia](https://pinia.vuejs.org/)
- [Socket.IO](https://socket.io/)

## Commands

### Docker containers management
- `bin/build` - rebuild docker containers
- `bin/start` - start the docker containers
- `bin/stop` - stop docker containers
- `bin/restart` - restart docker containers

### Service level commands
- `bin/node` - alias for node binary file in nodejs container
- `bin/bash` - opens bash console in nodejs container
- `bin/cli <command>` - run any cli command in nodejs container
- `bin/npm <command>` - run `npm` command in scope of nodejs container
- `bin/npx <command>` - run `npx` command in scope of nodejs container
- `bin/vue <command>` - run `vue` command in scope of nodejs container


## Getting started

### Environment configuration
Configuration uses .env file. It is not included in repository
and has to be set manually on every environment.

### Start application
1. Clone this repo
2. Copy .env.sample to .env
3. Run `bin/start` from project root to start application.
4. Application will start and print message to console.
5. Access the app at http://localhost:5051/ port number is defined in .env file

## FAQ

### Entrypoint
There's no need to rebuild the image unless there are changes to Dockerfile.
Nodejs image uses enrtypoint.sh to install npm dependencies and start the server.

No need add changes to docker file, just add them to entrypoint file

