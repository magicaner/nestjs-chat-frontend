import './assets/main.css'

import { createPinia } from 'pinia'
import { createApp } from 'vue';
import VueCookies from 'vue3-cookies'

const pinia = createPinia()
import App from './App.vue'

createApp(App)
    .use(pinia)
    .use(VueCookies)
    .mount('#app')
