import {defineStore} from 'pinia';
import type {LobbyStateType, UserType} from "@/store/types";
import {useChatStore} from "@/store/chat.store";
import {useAppStore} from "@/store/app.store";

export const useLobbyStore = defineStore('lobby', {
    state: () => {
        return {
            users: {},
            currentUser: null
        } as LobbyStateType
    },

    actions:{
        bootstrap(){

        },
        updateUser(user: UserType){
            this.users[user.id as string] = user;
        },
        resetUsers(users: UserType[]){
            this.users = {};

            users = users.sort((a, b) => a > b ? 1 : -1 );

            for (let user of users) {
                this.users[user.id as string] = user;
            }
        },
        selectUser(user: UserType) {
            const chatStore = useChatStore();
            chatStore.setCurrentUser(user);
        },
        getCurrentUserId() {
            const appStore = useAppStore();
            return appStore.userId;
        }
    }
})
