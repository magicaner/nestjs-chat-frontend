
export type UserType = {
    socketId: string;
    id: string;
    username?: string;
    isOnline?: boolean;
}
export type MessageType = {
    from?: string;
    to?: string;
    content?: string;
    timestamp?: Date;
}
export type AppStateType = {
    username?: string|null;
    password?: string|null;
    userId?: string|null;
    accessToken?: string|null;
}

export type LobbyStateType = {
    users: {[key:string] : UserType},
    currentUser: UserType | null
}

export type ChatStateType = {
    currentUser?: UserType | null,
    messages: MessageType[]
}

export type SessionType = {
    at: string;
    uid: string;
}

export type UserMessageType = {
    userId: string,
    messages: MessageType[]
}

export type MessagesStateType = {
    messages: {[key:string] : MessageType[]},
}
