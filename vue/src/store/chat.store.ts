import {defineStore} from 'pinia';
import type {ChatStateType, MessageType, UserType} from "@/store/types";
import {useMessagesStore} from "@/store/messages.store";

export const useChatStore = defineStore('chat', {
    state: () => {
        return {
            currentUser: null,
            messages: []
        } as ChatStateType
    },

    actions:{
        bootstrap(){

        },
        setCurrentUser(user: UserType)
        {
            this.currentUser = user;
        },
        receiveMessage(message: MessageType) {
            const messagesStore = useMessagesStore()
            if (message.from == <string>this.currentUser?.id) {
                messagesStore.receiveMessages(<UserType>this.currentUser, message);
            }
        },
        sendMessage(message: string){
            const messagesStore = useMessagesStore()
            if (!this.currentUser) {
                throw Error('Current user not found');
            }
            messagesStore.sendMessage(this.currentUser, message);
        },
        getMessages()
        {
            if (!this.currentUser) {
                throw Error('Current user not found');
            }
            const messagesStore = useMessagesStore()
            return messagesStore.getMessages(this.currentUser);
        },
        isVisible(): boolean {
            return !!this.currentUser;
        }
    }
})
