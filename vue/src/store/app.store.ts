import {defineStore} from 'pinia';
import {useCookies} from "vue3-cookies";
import {socketService} from "@/services/socket.service";
import {useLobbyStore} from "@/store/lobby.store";
import type {AppStateType, MessageType, SessionType, UserType} from "@/store/types";
import {useMessagesStore} from "@/store/messages.store";
import {useChatStore} from "@/store/chat.store";


export const useAppStore = defineStore('app', {
    state: () => {

        return {
            username: null,
            password: null,
            userId: null,
            accessToken: null,
        } as AppStateType
    },

    actions:{
        bootstrap(){


            const useLobby = useLobbyStore();
            const { cookies } = useCookies();

            socketService.on("lobby", (users: UserType[]) => {
                useLobby.resetUsers(users);
            });

            socketService.on("message", (message: MessageType) => {
                console.log(message);
                const chatStore = useChatStore()

                chatStore.receiveMessage(message);
            });

            try {
                const session: any|SessionType = cookies.get('session');
                this.accessToken = session.at ?? null;
                this.userId = session.uid ?? null;
                this.init();
            } catch (e) {
                console.log(e);
                // ignore errors
            }
        },
        init(){
            if (this.accessToken) {
                socketService.auth({
                    token: "Bearer " + this.accessToken
                });

                socketService.init();

                socketService.emit('init', {
                    accessToken: this.accessToken
                });
            }
        },
        async login (username: string, password: string) {
            const { cookies } = useCookies();
            const response = await fetch(import.meta.env.VITE_APP_URL + '/auth/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ username: username, password: password }),
            });

            const result = await response.json();
            this.accessToken = result.access_token;
            this.userId = result.user_id;


            const session = {
                at: <string>this.accessToken,
                uid: this.userId
            }

            cookies.set('session', JSON.stringify(session));

            this.init();
        },

    }
})
