import {defineStore} from 'pinia';
import type {MessagesStateType, MessageType, UserType} from "@/store/types";
import {useAppStore} from "@/store/app.store";
import {socketService} from "@/services/socket.service";

export const useMessagesStore = defineStore('messages', {
    state: () => {
        return {
            messages: {}
        } as MessagesStateType
    },

    actions:{
        bootstrap(){

        },
        getMessages(user: UserType) {
            return this.messages[user.id] ?? [];
        },
        receiveMessages(user: UserType, message: MessageType) {
            this.pushMessage(user, message);
        },
        sendMessage(user: UserType, message: string)
        {
            const appStore = useAppStore();

            const messageObject = {
                toSocketId: user.socketId,
                from: <string>appStore.userId,
                to: user.id,
                content: message,
                timestamp: new Date()
            };

            this.pushMessage(user, messageObject);

            socketService.emit('message', messageObject);
        },
        pushMessage(user: UserType, message: MessageType){
            if (!(user.id in this.messages)) {
                this.messages[user.id] = [];
            }
            this.messages[user.id].push(message);
        }
    }
})
