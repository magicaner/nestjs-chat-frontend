import {io, Socket} from "socket.io-client";

class SocketService
{
    public socket: Socket;
    constructor() {
        this.socket = io(import.meta.env.VITE_SOCKET_IO_URL, { autoConnect: false });
    }

    init() {
        this.socket.connect();

        this.socket.onAny((event: any, ...args: any[]) => {
            console.log('any log');
            console.log(event, args);
        });

        this.socket.on("connection", (socket: any) => {
            console.log('connected');
        });
    }

    on(event: any, listener: any){
        return this.socket.on(event, listener)
    }

    emit(event: any, ...args: any[]){
        return this.socket.emit(event, ...args);
    }

    auth(data: any){
        this.socket.auth = data;
        return this;
    }
}

export const socketService = new SocketService();
