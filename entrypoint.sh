#!/bin/bash
if [[ $NODE_ENV == "development" ]]; then
    if [[ ! -d "node_modules" ]]; then
        npm install --include dev
    fi
    rm -f tsconfig.tsbuildinfo
    npm run dev
else
    if [[ ! -d "node_modules" ]]; then
        npm install --include dev
    fi
    npm run build
fi
